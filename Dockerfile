FROM python:3.7.5-alpine3.10


RUN apk add --no-cache build-base && \
    pip install ryu==4.32 && \
    apk del build-base

RUN sed '/OFPFlowMod(/,/)/s/)/, table_id=1)/' \
        /usr/local/lib/python3.7/site-packages/ryu/app/simple_switch_13.py > \
        /usr/local/lib/python3.7/site-packages/ryu/app/qos_simple_switch_13.py

EXPOSE 8080 6653

CMD ["ryu-manager", \
    "ryu.app.rest_qos", \ 
    "ryu.app.qos_simple_switch_13", \
    "ryu.app.rest_conf_switch"]

# ryu-docker

Docker image for [Ryu SDN Controller](https://osrg.github.io/ryu/) with QoS support.

```
docker pull dlartigae/ryu:latest
```

Run
```
docker run -d -p 6653:6653 -p 8080:8080 --name ryu dlartigae/ryu:latest
```

With [docker compose](https://docs.docker.com/compose/)

Run
```
docker-compose up -d
```

## Ports

- `6653`: Controller
- `8080`: API REST

## Dockerhub

[https://hub.docker.com/r/dlartigae/ryu](https://hub.docker.com/r/dlartigae/ryu)
